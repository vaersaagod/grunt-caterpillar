# grunt-caterpillar

> A Grunt plugin to find and pass custom filesets to grunt-contrib-uglify.

## Getting Started
This plugin requires Grunt `~0.4.5`

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:

```shell
npm install grunt-caterpillar --save-dev
```

Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:

```js
grunt.loadNpmTasks('grunt-caterpillar');
```

## The "caterpillar" task

### Overview
In your project's Gruntfile, add a section named `caterpillar` to the data object passed into `grunt.initConfig()`.

```js
grunt.initConfig({
  caterpillar: {
    options: {
      // Options to be passed on to grunt-contrib-uglify.
    },
    your_target: {
      // Target-specific file lists and/or options go here.
    },
  },
});
```

### Options
See [grunt-contrib-uglify](https://github.com/gruntjs/grunt-contrib-uglify#options).

### Usage Examples

#### Markup
```html
<IF LOCAL>
  <!-- caterpillar out="assets/build/js/compiled.js" -->
  <script src="/assets/src/js/libs/TweenMax.js"></script>
  <script src="/assets/src/js/libs/EasePack.js"></script>
  <script src="/assets/src/js/libs/CSSPlugin.js"></script>
  <script src="/assets/src/js/main.js"></script>
  <!-- /caterpillar -->
<ELSE>
  <script src="assets/build/js/compiled.js"></script>
<END>
```

```js
grunt.initConfig({
  caterpillar: {
    options: {
      preserveComments: 'some'
    },
    your_target: {
      src: 'assets/**/*.{twig,html,tpl,php,jsp,cshtml}'
    }
  }
});
```

## Contributing
In lieu of a formal styleguide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code using [Grunt](http://gruntjs.com/).

## Release History
_(Nothing yet)_
