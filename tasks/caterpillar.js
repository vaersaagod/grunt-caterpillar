/*
 * grunt-caterpillar
 * 
 *
 * Copyright (c) 2014 Alexander Bech
 * Licensed under the MIT license.
 */

 'use strict';

 module.exports = function(grunt) {

  var chalk = require('chalk'),
      path = require('path');

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.registerMultiTask('caterpillar', 'Look for supported file-sets in source files and pass them on to uglify', function() {

    var blockRegexp = /(?:<!-- caterpillar)([\s\S]+?)(?:<!-- \/caterpillar -->)/gim;
    var pathsRegexp = /(?:dest|out|href|src)=["']([^'"]+)["']/gim;
    var filesets = [];
    var match;
    var sourceFiles = this.filesSrc;
    var sourceFilesContent = '';

    grunt.log.writeln( chalk.red( grunt.file.read( __dirname + path.sep + '.caterpillar') ) );
    grunt.log.writeln('Crawling over your files looking for something to eat...\n');

    sourceFiles.forEach(function(sourceFilePath) {
      sourceFilesContent = grunt.file.read(sourceFilePath);
      while ((match = blockRegexp.exec(sourceFilesContent))) {
        grunt.log.writeln(chalk.green('✔ ') + sourceFilePath);
        var filesetMatch = match[1];
        var files = [];
        var fileset = {};
        while ((match = pathsRegexp.exec(filesetMatch))) {
          var filepath = match[1];
          if (filepath.charAt(0) === "/") {
            filepath = filepath.substr(1);
          }
          if (files.length) {
            grunt.log.writeln(chalk.cyan('    → ' + filepath));
          }
          files.push(filepath);
        }
        grunt.log.writeln(chalk.green('    ← ' + files[0]));
        fileset[files.shift()] = files;
        filesets.push(fileset);
      }
    });

    if (filesets.length) {
      var task = {
        options : this.options(),
        caterpillar: {
          files: filesets
        }
      };
      grunt.config.set('uglify', task);
      grunt.task.run('uglify');
    } else {
      grunt.log.writeln(chalk.red('✘ ') + 'no supported file-sets found in source files!');
    }
  });

};
